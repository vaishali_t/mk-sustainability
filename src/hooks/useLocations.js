import { graphql, useStaticQuery } from "gatsby"

const useLocations = () => {
  const data = useStaticQuery(graphql`
    query {
      allLocationsCsv {
        nodes {
          lat_
          long_
          link
          title_
          address_
          postcode_
          googlemaps_url
          category_
          material_tags_
          description
          image
          additional_information
        }
      }
    }
  `)

  return data.allLocationsCsv.nodes.map(l => ({
    lat: l.lat_,
    long: l.long_,
    link: l.link,
    title: l.title_,
    address: l.address_,
    postcode: l.postcode_,
    googlemapsUrl: l.googlemaps_url,
    category: (l.category_ ?? "").split(",")[0].trim(),
    materialTags: (l.material_tags_ ?? "").split(",").map(t => t.trim()),
    description: l.description,
    image: l.image,
    additionalInformation: l.additional_information,
  }))
}

export default useLocations
