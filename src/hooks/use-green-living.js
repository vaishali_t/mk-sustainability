import { graphql, useStaticQuery } from "gatsby"

const useGreenLiving = () => {
  const data = useStaticQuery(graphql`
    query {
      allMdx(filter: { fields: { collection: { eq: "green-living" } } }) {
        nodes {
          slug
          body
          frontmatter {
            title
            url
            tags
          }
        }
      }
    }
  `)

  return data.allMdx.nodes.map(gl => ({
    ...gl,
    ...gl.frontmatter,
    frontmatter: undefined,
  }))
}

export default useGreenLiving
