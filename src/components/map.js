import React from "react"
import { Helmet } from "react-helmet"
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet"
import useLocations from "../hooks/useLocations"
import categories from "./categories"
import { useHasMounted } from "../hooks/useHasMounted"
import styled from "@emotion/styled"

const [icons] =
  typeof window !== "undefined"
    ? (() => {
        const L = require("leaflet")
        const mkIcon = url => new L.Icon({ iconUrl: url })

        const icons = Object.assign(
          {},
          ...categories.map(cat => ({
            [cat.id]: mkIcon(cat.icon),
          }))
        )

        return [icons]
      })()
    : [{}]

const lookupCategory = categoryCsvName =>
  categories.find(cat => cat.csvName === categoryCsvName)

const lookupIcon = categoryId =>
  categoryId in icons ? icons[categoryId] : null

const PopupStyled = styled(Popup)`
  .popup {
    display: grid;
    grid-template:
      "a b"
      "a c"
      "d d"
      "e e"
      "f f"
      "g g";
    align-content: flex-start;
    justify-items: flex-start;
    grid-template-columns: auto 1fr;
    grid-column-gap: 1.5em;
    grid-row-gap: 0.5em;
  }

  .image {
    grid-area: a;
    width: 60px;
    height: 60px;
    border-radius: 30px;
    border: 3px solid green;
    background-color: #555;
  }

  h3 {
    grid-area: b;
  }

  .address {
    grid-area: c;
    display: flex;
    flex-direction: column;
  }

  .description {
    grid-area: d;
  }

  .site-url {
    grid-area: e;
  }
  .gmaps-url {
    grid-area: f;
  }
  .tags {
    grid-area: g;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: start;
    flex-wrap: wrap;

    span {
      margin-right: 1em;
    }
  }
`

const Map = ({ selectedCategories }) => {
  const locations = useLocations()
  const hasMounted = useHasMounted()

  const markers = locations
    .map(loc => {
      const category = lookupCategory(loc.category)
      return {
        pos: [loc.lat, loc.long],
        category: category,
        icon: category ? lookupIcon(category.id) : null,
        popup: (
          <div className="popup">
            <div className="image"></div>
            <h3>{loc.title}</h3>
            <div className="address">
              {loc.address.split(",").map((part, i) => (
                <span key={i}>{part}</span>
              ))}{" "}
              <span>{loc.postcode}</span>
            </div>
            <div className="description">{loc.description}</div>
            {loc.link ? (
              <div className="site-url">
                <a href={loc.link}>Website</a>
              </div>
            ) : null}
            {loc.googlemapsUrl ? (
              <div className="gmaps-url">
                <a href={loc.googlemapsUrl}>Google Maps</a>
              </div>
            ) : null}
            <div className="tags">
              {loc.materialTags.map((t, i) => (
                <span key={i}>#{t}</span>
              ))}
            </div>
          </div>
        ),
      }
    })
    .filter(
      marker =>
        marker.icon !== null &&
        selectedCategories.some(cat => cat.id === marker.category.id)
    )

  return (
    <>
      <Helmet>
        <link
          rel="stylesheet"
          href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
          integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
          crossorigin=""
        />
        <script
          src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
          integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
          crossorigin=""
        ></script>
      </Helmet>
      {hasMounted ? (
        <MapContainer
          center={[52.04193111233155, -0.7592732391895334]}
          zoom={13}
          scrollWheelZoom={true}
          style={{ height: "100%", width: "100%", flexGrow: 1, flexShrink: 1 }}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            // url="'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}'"
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            accessToken="eyJ1IjoiamFuLWRhIiwiYSI6ImNrbWdnemR0cjA5YW0yeXFsZW40ZDQ4dmMifQ.I_pSf5wxhQs4Wi3sYfiV_A"
            id="mapbox/streets-v11"
          />

          {markers.map(({ pos, icon, popup }, i) => (
            <Marker key={i} position={pos} icon={icon}>
              {popup ? <PopupStyled>{popup}</PopupStyled> : null}
            </Marker>
          ))}
        </MapContainer>
      ) : null}
    </>
  )
}

export default Map
