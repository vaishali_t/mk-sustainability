import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import styled from "@emotion/styled"
import SustainableMkSvg from "../../images/sustainablemk.svg"

const HeaderStyled = styled.header`
  height: 110px;
  border: 1px solid #eaeaea;
  box-shadow: 0 2px 20px 0px rgba(0, 0, 0, 10%);

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  padding-left: 40px;
  padding-right: 40px;

  font-family: Faune, Arial, sans-serif;

  a,
  a:visited {
    text-decoration: none;
    color: black;

    &.current {
      color: #d55e42;
    }
  }

  h1 {
    img {
    }
  }

  > nav {
    font-size: 1.5rem;
    font-weight: 700;

    > *:first-of-type {
      margin-left: 0;
    }

    > * {
      margin-left: 40px;
    }
  }
`
const Header = ({ siteTitle }) => (
  <HeaderStyled>
    <h1>
      <Link
        to="/"
        style={{
          color: `white`,
          textDecoration: `none`,
        }}
      >
        <img src={SustainableMkSvg} alt="Sustainable MK" />
      </Link>
    </h1>
    <nav>
      <Link activeClassName="current" to="/">
        Locations
      </Link>
      <Link activeClassName="current" to="/green-living">
        Green Living
      </Link>
      <Link activeClassName="current" to="/about">
        About
      </Link>
    </nav>
  </HeaderStyled>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
