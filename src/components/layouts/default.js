/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import { Global, css } from "@emotion/react"
import Header from "./header"
import { Helmet } from "react-helmet"
import Footer from "./footer"
import "../../fonts/faune/type-faune.css"

const globalStyle = css`
          * {
            box-sizing: border-box;
            margin: 0;
          }

          html,
          body {
            margin: 0;
            color: #555;
            font-family: Lato, Arial, sans-serif;
            font-size: 1em;
            line-height: 1.4;

            > div {
              margin-top: 0;
            }
          }

          #gatsby-focus-wrapper {
            height: 100%;
          }

          .content-root {
            height: 100vh;
            display: flex;
            flex-direction: column;
            align-items: stretch;
          }

          main {
            ${"" /* height: calc(100vh - 110px - 80px); */}
            position: relative;
            flex-basis: 1;
            flex-shrink: 1;
            flex-grow: 1;
          }

          a, a:visited {
            text-decoration: underline;
            color: #D55E42;
          }
        }`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <Global styles={globalStyle} />
      <Helmet>
        <link
          href="https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext"
          rel="stylesheet"
          type="text/css"
        />
      </Helmet>
      <div className="content-root">
        <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
        <main>{children}</main>
        <Footer />
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
