import React from "react"
import styled from "@emotion/styled"
import { Link } from "gatsby"
import SustainableMkIdentSvg from "../../images/sustainablemk-ident.svg"

const LinkStyled = styled(Link)``

const FooterWrapper = styled.footer`
  height: 80px;
  background-color: #dfdfdf;
  border: 2px solid #cdcdcd;

  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  .link-home {
    position: absolute;
    top: -34px;
    left: 40px;
    z-index: 999;
  }

  .link-about {
    font-family: Faune, Arial, sans-serif;
    font-weight: 700;
    font-size: 2.5rem;
  }
`
const Footer = () => (
  <FooterWrapper>
    <Link className="link-home" to="/">
      <img src={SustainableMkIdentSvg} alt="footer logo" />
    </Link>
    <LinkStyled className="link-about" to="/about">
      What is Sustainable MK all about?
    </LinkStyled>
  </FooterWrapper>
)

export default Footer
