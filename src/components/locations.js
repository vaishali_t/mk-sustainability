import styled from "@emotion/styled"
import { Link } from "gatsby"
import React, { useState } from "react"
import Map from "./map"
import categories from "./categories"
import { useHasMounted } from "../hooks/useHasMounted"

const CategorySelectorWrapper = styled.label`
  height: 54px;

  border-radius: 32px;
  border: 3px solid transparent;

  display: inline;

  &.checked {
    border: 3px solid ${({ color }) => (color ? color : "black")};
  }

  position: relative;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  margin-left: -6px;

  font-weight: 700;
  color: ${({ color }) => (color ? color : "black")};

  input {
    display: block;
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0;
  }

  img {
    margin: 3px 0 3px 3px;
  }

  img + span {
    margin-left: 8px;
  }
`

const CategorySelector = ({ icon, children, color, checked, setChecked }) => {
  return (
    <CategorySelectorWrapper
      className={checked ? "checked" : "non-checked"}
      color={color}
    >
      <input
        type="checkbox"
        checked={checked}
        onChange={e => setChecked(e.target.checked)}
      />
      <img src={icon} role="presentation" alt="" />
      <span>{children}</span>
    </CategorySelectorWrapper>
  )
}

const LocationsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  overflow: hidden;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;

  font-size: 1.125rem;

  .map-menu {
    width: 390px;
    height: 100%;
    overflow-y: auto;
    background-color: white;

    padding: 2rem 30px 1rem 30px;

    display: flex;
    flex-direction: column;

    h2 {
      font-family: Faune, Arial, sans-serif;
      margin-bottom: 1rem;
    }

    ${CategorySelectorWrapper} {
      margin-top: 10px;
    }

    p {
      margin-top: 1.5rem;
    }
  }
`
const Locations = () => {
  const hasMounted = useHasMounted()
  const [selection, setSelection] = useState(() => [
    ...categories.map(cat => ({
      category: cat,
      selected: false,
    })),
  ])

  const changeSelection = (id, value) => {
    setSelection(old =>
      old.map(sel =>
        sel.category.id !== id ? sel : { ...sel, selected: value }
      )
    )
  }

  const selectedCategories = selection
    .filter(sel => sel.selected)
    .map(sel => sel.category)

  return (
    <LocationsWrapper>
      {hasMounted ? <Map selectedCategories={selectedCategories} /> : <div />}
      <div className="map-menu">
        <h2>What would you like to see?</h2>
        {selection.map(
          ({ category: { id, display, icon, color }, selected }) => (
            <CategorySelector
              key={id}
              icon={icon}
              color={color}
              checked={selected}
              setChecked={value => changeSelection(id, value)}
            >
              {display}
            </CategorySelector>
          )
        )}
        <p>
          Can't find what you're looking for? Why not have a look on our{" "}
          <Link to="/green-living">Green Living</Link> page
        </p>
      </div>
    </LocationsWrapper>
  )
}

export default Locations
