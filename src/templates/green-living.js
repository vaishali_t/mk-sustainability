import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layouts/green-living"
import { MDXRenderer } from "gatsby-plugin-mdx"

export const query = graphql`
  query($slug: String!) {
    mdx(fields: { collection: { eq: "green-living" } }, slug: { eq: $slug }) {
      slug
      body
      frontmatter {
        title
        url
        tags
      }
    }
  }
`

const GreenLivingTemplate = ({ data: { mdx: glPage } }) => (
  <Layout>
    <h1>{glPage.frontmatter.title}</h1>
    <MDXRenderer>{glPage.body}</MDXRenderer>
  </Layout>
)

export default GreenLivingTemplate
