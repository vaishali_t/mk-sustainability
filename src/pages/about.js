import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layouts/default"
export default function About({ data }) {
  return (
    <Layout>
      <h1>About {data.site.siteMetadata.title}</h1>
      <p>
        A small site for how to live more sustainably in Milton Keynes. 
        Developed during the <a href="https://protospace.uk/events/sustainability-hackathon">sustainability hackathon</a> by <a href="https://www.mkhackathon.org/">MK Hackathon</a> and <a href="https://protospace.uk/">MK Protospace</a>.
      </p>
    </Layout>
  )
}
export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
  `