/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require("path")
const _ = require("lodash")

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === `Mdx`) {
    const parent = getNode(node.parent)
    let collection = parent.sourceInstanceName
    createNodeField({
      node,
      name: "collection",
      value: collection,
    })
  }
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  const tagTemplate = path.resolve("src/templates/tags.js")
  const glPageMdxs = await graphql(`
    query {
      allMdx(filter: { fields: { collection: { eq: "green-living" } } }
      ) {
          nodes {
            slug
            body
            frontmatter {
              title
              url
              category
              tags
            }
          }
        }
        tagsGroup: allMdx(limit: 2000) {
          group(field: frontmatter___tags) {
            fieldValue
        }
      }
    }
  `)


  if (glPageMdxs.errors) {
    reporter.panic("failed to create green-living pages", glPageMdxs.errors)
  }

  const glPages = glPageMdxs.data.allMdx.nodes

  glPages.forEach(glPage => {
    actions.createPage({
      path: `/green-living/${glPage.slug}`,
      component: require.resolve("./src/templates/green-living.js"),
      context: {
        slug: glPage.slug,
      },
    })
  })

  // Extract tag data from query
  const tags = glPageMdxs.data.tagsGroup.group
  // Make tag pages
  tags.forEach(tag => {
    actions.createPage({
      path: `/tags/${_.kebabCase(tag.fieldValue)}/`,
      component: tagTemplate,
      context: {
        tag: tag.fieldValue,
        slug: tag.fieldValue,
      },
    })
  })

}

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html" || stage === "develop-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /leaflet-src/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}
